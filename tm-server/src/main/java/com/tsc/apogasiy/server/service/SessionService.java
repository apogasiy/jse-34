package com.tsc.apogasiy.server.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsc.apogasiy.server.api.repository.ISessionRepository;
import com.tsc.apogasiy.server.api.service.IPropertyService;
import com.tsc.apogasiy.server.api.service.ISessionService;
import com.tsc.apogasiy.server.api.service.IUserService;
import com.tsc.apogasiy.server.enumerated.Role;
import com.tsc.apogasiy.server.exception.empty.EmptyIdException;
import com.tsc.apogasiy.server.exception.empty.EmptyUserIdException;
import com.tsc.apogasiy.server.exception.system.AccessDeniedException;
import com.tsc.apogasiy.server.model.Session;
import com.tsc.apogasiy.server.model.User;
import com.tsc.apogasiy.server.util.HashUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.naming.spi.ObjectFactory;
import java.util.Objects;
import java.util.Optional;

public class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    public SessionService(@NotNull final ISessionRepository sessionRepository, @NotNull IUserService userService, @NotNull IPropertyService propertyService) {
        super(sessionRepository);
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @Override
    public @NotNull Session open(@NotNull final String login, @NotNull final String password) {
        if (checkAuth(login, password)) {
            @NotNull final User user = userService.findByLogin(login);
            @NotNull final Session session = new Session();
            session.setUserId(user.getId());
            session.setTimestamp(System.currentTimeMillis());
            repository.add(session);
            return sign(session);
        }
        else
            return null;
    }

    @Override
    public @Nullable Session close(@NotNull final Session session) {
        return repository.removeById(session.getId());
    }

    private boolean checkAuth(@NotNull final String login, @NotNull final String password) {
        @NotNull final User user = userService.findByLogin(login);
        @NotNull final String hashedPassword = HashUtil.encrypt(propertyService, password);
        return hashedPassword.equals(user.getHashedPassword());
    }

    public void validate(@Nullable final Session session) {
        @NotNull final Session externalSession = Optional.ofNullable(session).filter(Session::isReqAttrFilled).orElseThrow(AccessDeniedException::new);
        @NotNull final Session sampleSession = sign(externalSession.clone());
        if (!sampleSession.getSignature().equals(externalSession.getSignature())
                || !repository.contains(session.getId()))
            throw new AccessDeniedException();
    }

    @Override
    public void validateAdmin(@Nullable Session session) {
        validate(session);
        if (!(userService.findById(session.getUserId()).getRole() == Role.ADMIN))
            throw new AccessDeniedException();
    }

    @SneakyThrows
    private @NotNull Session sign(@NotNull final Session session) {
        session.resetSignature();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        session.setSignature(HashUtil.encrypt(propertyService, objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(session)));
        return session;
    }

}
