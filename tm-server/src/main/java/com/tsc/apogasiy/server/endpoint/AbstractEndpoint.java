package com.tsc.apogasiy.server.endpoint;

import com.tsc.apogasiy.server.api.service.IServiceLocator;
import com.tsc.apogasiy.server.api.service.ISessionService;
import com.tsc.apogasiy.server.model.Session;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.ws.Endpoint;

@Getter
@Setter
public abstract class AbstractEndpoint {

    protected @NotNull final IServiceLocator serviceLocator;
    protected @Nullable Endpoint endpoint;

    public AbstractEndpoint(@NotNull IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    protected void validateSession(@Nullable final Session session) {
        serviceLocator.getSessionService().validate(session);
    }

    protected void validateAdminSession(@Nullable final Session session) {
        serviceLocator.getSessionService().validateAdmin(session);
    }

}
