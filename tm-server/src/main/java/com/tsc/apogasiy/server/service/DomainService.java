package com.tsc.apogasiy.server.service;

import com.tsc.apogasiy.server.api.service.IDomainService;
import com.tsc.apogasiy.server.api.service.IServiceLocator;
import com.tsc.apogasiy.server.dto.Domain;
import com.tsc.apogasiy.server.exception.entity.EntityNotFoundException;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class DomainService implements IDomainService {

    @NotNull
    @Getter
    final IServiceLocator bootstrap;

    public DomainService(@NotNull IServiceLocator bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public @NotNull Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        if (!Optional.ofNullable(bootstrap).isPresent())
            throw new EntityNotFoundException();
        domain.setProjects(bootstrap.getProjectService().findAll());
        domain.setTasks(bootstrap.getTaskService().findAll());
        domain.setUsers(bootstrap.getUserService().findAll());
        return domain;
    }

    @Override
    public void setDomain(@Nullable Domain domain) {
        if (!Optional.ofNullable(bootstrap).isPresent())
            throw new EntityNotFoundException();
        if (Optional.ofNullable(domain).isPresent()) {
            bootstrap.getUserService().clear();
            bootstrap.getTaskService().clear();
            bootstrap.getProjectService().clear();
            bootstrap.getUserService().addAll(domain.getUsers());
            bootstrap.getTaskService().addAll(domain.getTasks());
            bootstrap.getProjectService().addAll(domain.getProjects());
            if (bootstrap.getAuthService().isAuth())
                bootstrap.getAuthService().logout();
        }

    }

}
