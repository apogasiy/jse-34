package com.tsc.apogasiy.server.dto;

import com.tsc.apogasiy.server.model.Project;
import com.tsc.apogasiy.server.model.Task;
import com.tsc.apogasiy.server.model.User;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@XmlRootElement(name = "domain")
@XmlAccessorType(XmlAccessType.FIELD)
public class Domain implements Serializable {

    @NotNull
    @XmlElement(name = "project")
    @XmlElementWrapper(name = "projects")
    private List<Project> projects;

    @NotNull
    @XmlElement(name = "task")
    @XmlElementWrapper(name = "tasks")
    private List<Task> tasks;

    @NotNull
    @XmlElement(name = "user")
    @XmlElementWrapper(name = "users")
    private List<User> users;

}
