package com.tsc.apogasiy.server.repository;

import com.tsc.apogasiy.server.api.repository.IUserRepository;
import com.tsc.apogasiy.server.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    @Nullable
    public User findByLogin(@NotNull final String login) {
        return list.stream()
                .filter(usr -> usr.getLogin().toLowerCase().equals(login.toLowerCase()))
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public User findByEmail(@NotNull final String email) {
        return list.stream()
                .filter(usr -> usr.getEmail().toLowerCase().equals(email.toLowerCase()))
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public User removeByLogin(@NotNull final String login) {
        @Nullable final User user = findByLogin(login);
        remove(user);
        return user;
    }

    @Override
    public boolean userExistsByLogin(@NotNull final String login) {
        return findByLogin(login) != null;
    }

    @Override
    public boolean userExistsByEmail(@NotNull final String email) {
        return findByEmail(email) != null;
    }

}
