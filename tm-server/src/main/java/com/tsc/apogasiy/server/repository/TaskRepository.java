package com.tsc.apogasiy.server.repository;

import com.tsc.apogasiy.server.enumerated.Status;
import com.tsc.apogasiy.server.api.repository.ITaskRepository;
import com.tsc.apogasiy.server.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    @Override
    @NotNull
    public List<Task> findAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return findAll(userId).stream()
                .filter(t -> t.getProjectId().equals(projectId))
                .collect(Collectors.toList());
    }

    @Override
    @Nullable
    public Task findByName(@NotNull final String userId, @NotNull final String name) {
        return findAll(userId).stream()
                .filter(t -> t.getName().equals(name))
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public Task removeByName(@NotNull final String userId, @NotNull final String name) {
        final Task task = findByName(userId, name);
        if (!Optional.ofNullable(task).isPresent())
            return null;
        list.remove(task);
        return task;
    }

    @Override
    public boolean existsByName(@NotNull final String userId, @NotNull final String name) {
        return findByName(userId, name) != null;
    }

    @Override
    public boolean existsByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return findByIndex(userId, index) != null;
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findById(userId, id) != null;
    }

    @Override
    @Nullable
    public Task startById(@NotNull final String userId, @NotNull final String id) {
        final Task task = findById(userId, id);
        if (!Optional.ofNullable(task).isPresent())
            return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    @Nullable
    public Task startByIndex(@NotNull final String userId, @NotNull final Integer index) {
        final Task task = findByIndex(userId, index);
        if (!Optional.ofNullable(task).isPresent())
            return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    @Nullable
    public Task startByName(@NotNull final String userId, @NotNull final String name) {
        final Task task = findByName(userId, name);
        if (!Optional.ofNullable(task).isPresent())
            return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    @Nullable
    public Task finishById(@NotNull final String userId, @NotNull final String id) {
        final Task task = findById(userId, id);
        if (!Optional.ofNullable(task).isPresent())
            return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    @Nullable
    public Task finishByIndex(@NotNull final String userId, @NotNull final Integer index) {
        final Task task = findByIndex(userId, index);
        if (!Optional.ofNullable(task).isPresent())
            return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    @Nullable
    public Task finishByName(@NotNull final String userId, @NotNull final String name) {
        final Task task = findByName(userId, name);
        if (!Optional.ofNullable(task).isPresent())
            return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    @Nullable
    public Task changeStatusById(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) {
        final Task task = findById(userId, id);
        if (!Optional.ofNullable(task).isPresent())
            return null;
        task.setStatus(status);
        return task;
    }

    @Override
    @Nullable
    public Task changeStatusByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final Status status) {
        final Task task = findByIndex(userId, index);
        if (!Optional.ofNullable(task).isPresent())
            return null;
        task.setStatus(status);
        return task;
    }

    @Override
    @Nullable
    public Task changeStatusByName(@NotNull final String userId, @NotNull final String name, @NotNull final Status status) {
        final Task task = findByName(userId, name);
        if (!Optional.ofNullable(task).isPresent())
            return null;
        task.setStatus(status);
        return task;
    }

    @Override
    @Nullable
    public Task bindTaskToProjectById(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) {
        final Task task = findById(userId, taskId);
        if (!Optional.ofNullable(task).isPresent())
            return null;
        task.setProjectId(projectId);
        return task;
    }

    @Override
    @Nullable
    public Task unbindTaskById(@NotNull final String userId, @NotNull final String taskId) {
        final Task task = findById(userId, taskId);
        if (!Optional.ofNullable(task).isPresent())
            return null;
        task.setProjectId(null);
        task.setUserId(null);
        return task;
    }

    @Override
    public void removeAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        findAll(userId).stream()
                .filter(t -> projectId.equals(t.getProjectId()))
                .forEach(t -> t.setProjectId(null));
    }

}
