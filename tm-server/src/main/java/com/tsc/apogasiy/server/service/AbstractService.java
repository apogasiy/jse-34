package com.tsc.apogasiy.server.service;

import com.tsc.apogasiy.server.exception.entity.EntityNotFoundException;
import com.tsc.apogasiy.server.exception.system.IndexIncorrectException;
import com.tsc.apogasiy.server.model.AbstractEntity;
import com.tsc.apogasiy.server.api.repository.IRepository;
import com.tsc.apogasiy.server.api.service.IService;
import com.tsc.apogasiy.server.exception.empty.EmptyIdException;
import com.tsc.apogasiy.server.exception.empty.EmptyIndexException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @Nullable
    protected final IRepository<E> repository;

    public AbstractService(final IRepository<E> repository) {
        this.repository = repository;
    }

    @Nullable
    public void add(@Nullable final E entity) {
        Optional.ofNullable(entity).orElseThrow(EntityNotFoundException::new);
        repository.add(entity);
    }

    public void addAll(@Nullable List<E> entities) {
        if (!Optional.ofNullable(entities).isPresent())
            throw new EntityNotFoundException();
        repository.addAll(entities);
    }

    public void remove(@Nullable final E entity) {
        Optional.ofNullable(entity).orElseThrow(EntityNotFoundException::new);
        repository.remove(entity);
    }

    @NotNull
    public List<E> findAll() {
        return repository.findAll();
    }

    @NotNull
    public List<E> findAll(@Nullable final Comparator<E> comparator) {
        if (comparator == null)
            return Collections.emptyList();
        else
            return repository.findAll(comparator);
    }

    public void clear() {
        repository.clear();
    }

    public boolean isEmpty() {
        return repository.isEmpty();
    }

    @Nullable
    public E findById(@Nullable String id) {
        if (!Optional.ofNullable(id).isPresent() || id.isEmpty())
            throw new EmptyIdException();
        return repository.findById(id);
    }

    @Nullable
    public E findByIndex(@Nullable final Integer index) {
        Optional.ofNullable(index).orElseThrow(EmptyIndexException::new);
        if (index < 0)
            throw new IndexIncorrectException();
        return repository.findByIndex(index);
    }

    public boolean existsById(@Nullable final String id) {
        if (!Optional.ofNullable(id).isPresent() || id.isEmpty())
            throw new EmptyIdException();
        return repository.existsById(id);
    }

    public boolean existsByIndex(@NotNull final Integer index) {
        return repository.existsByIndex(index);
    }

    @Nullable
    public E removeById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        return repository.removeById(id);
    }

    @Nullable
    public E removeByIndex(@Nullable final Integer index) {
        Optional.ofNullable(index).orElseThrow(EmptyIndexException::new);
        return repository.removeByIndex(index);
    }

    public boolean contains(@NotNull final String id) {
        return repository.contains(id);
    }

}
