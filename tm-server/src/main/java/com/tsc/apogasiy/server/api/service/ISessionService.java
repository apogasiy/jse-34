package com.tsc.apogasiy.server.api.service;

import com.tsc.apogasiy.server.model.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface ISessionService extends IService<Session>{

    @NotNull
    Session open(@NotNull final String login, @NotNull final String password);

    @Nullable
    Session close(@NotNull final Session session);

    void validate(@Nullable final Session session);

    void validateAdmin(@Nullable final Session session);

}
