package com.tsc.apogasiy.server.exception;

public class AbstractException extends RuntimeException {

    public AbstractException() {
        super();
    }

    public AbstractException(String message, Throwable cause) {
        super(message, cause);
    }

    public AbstractException(Throwable cause) {
        super(cause);
    }

    public AbstractException(String s) {
        super(s);
    }

}
