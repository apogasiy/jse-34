package com.tsc.apogasiy.server.api.service;

import com.tsc.apogasiy.server.enumerated.Status;
import com.tsc.apogasiy.server.model.Task;
import org.jetbrains.annotations.Nullable;

public interface ITaskService extends IOwnerService<Task> {

    void create(@Nullable final String userId, @Nullable final String name);

    void create(@Nullable final String userId, @Nullable final String name, @Nullable final String description);

    @Nullable
    Task findByName(@Nullable final String userId, @Nullable final String name);

    @Nullable
    Task findByIndex(@Nullable final String userId, @Nullable final Integer index);

    @Nullable
    Task removeByName(@Nullable final String userId, @Nullable final String name);

    @Nullable
    Task updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description);

    @Nullable
    Task updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description);

    boolean existsByName(@Nullable final String userId, @Nullable final String name);

    @Nullable
    Task startById(@Nullable final String userId, @Nullable final String id);

    @Nullable
    Task startByIndex(@Nullable final String userId, @Nullable final Integer index);

    @Nullable
    Task startByName(@Nullable final String userId, @Nullable final String name);

    @Nullable
    Task finishById(@Nullable final String userId, @Nullable final String id);

    @Nullable
    Task finishByIndex(@Nullable final String userId, @Nullable final Integer index);

    @Nullable
    Task finishByName(@Nullable final String userId, @Nullable final String name);

    @Nullable
    Task changeStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status);

    @Nullable
    Task changeStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status);

    @Nullable
    Task changeStatusByName(@Nullable final String userId, @Nullable final String name, @Nullable final Status status);

}
