package com.tsc.apogasiy.server.component;

import com.tsc.apogasiy.server.api.repository.*;
import com.tsc.apogasiy.server.api.service.*;
import com.tsc.apogasiy.server.command.AbstractCommand;
import com.tsc.apogasiy.server.command.system.ExitCommand;
import com.tsc.apogasiy.server.endpoint.*;
import com.tsc.apogasiy.server.enumerated.Role;
import com.tsc.apogasiy.server.exception.system.UnknownCommandException;
import com.tsc.apogasiy.server.repository.*;
import com.tsc.apogasiy.server.service.*;
import com.tsc.apogasiy.server.util.HashUtil;
import com.tsc.apogasiy.server.model.Project;
import com.tsc.apogasiy.server.model.Task;
import com.tsc.apogasiy.server.model.User;
import com.tsc.apogasiy.server.util.SystemUtil;
import com.tsc.apogasiy.server.util.TerminalUtil;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;

public class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    @NotNull
    private final ILogService logService = new LogService();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    private final IAuthRepository authRepository = new AuthRepository();

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(authRepository, userService);

    @Getter
    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository, userService, propertyService);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    @NotNull
    private final UserAdminEndpoint userAdminEndpoint = new UserAdminEndpoint(this);

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpoint(this);

    public void start(final String... args) {
        initPID();
        displayWelcome();
        initTestData();
        initCommands();
        initBackup();
        initEndpoint();
        logService.debug("Server started");

        @Nullable String command = "";
        while (!command.equals(ExitCommand.NAME)) {
            try {
                System.out.println("Type \"exit\" for stop server");
                command = TerminalUtil.nextLine();
                runCommand(command);
            } catch (Exception e) {
                logService.error(e);
            }
        }
        logService.info("Shutdown server");
        stopEndpoint();
        stopBackup();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initBackup() {
        backup.load();
        backup.start();
    }

    private void initEndpoint() {
        publishEndpoint(projectEndpoint);
        publishEndpoint(taskEndpoint);
        publishEndpoint(sessionEndpoint);
        publishEndpoint(userAdminEndpoint);
        publishEndpoint(userEndpoint);
    }

    private void publishEndpoint(@NotNull final AbstractEndpoint endpoint) {
        final String wsdlUrl = "http://"+getPropertyService().getServerHost()+":"+getPropertyService().getServerPort()+"/"+endpoint.getClass().getSimpleName();
        System.out.println(wsdlUrl);
        endpoint.setEndpoint(Endpoint.publish(wsdlUrl, endpoint));
    }

    private void stopEndpoint() {
        stopEndpoint(projectEndpoint);
        stopEndpoint(taskEndpoint);
        stopEndpoint(sessionEndpoint);
        stopEndpoint(userAdminEndpoint);
        stopEndpoint(userEndpoint);
    }

    private void stopEndpoint(@NotNull final AbstractEndpoint endpoint) {
        @Nullable final Endpoint ep = endpoint.getEndpoint();
        if (ep != null && ep.isPublished())
            ep.stop();
    }

    private void stopBackup() {
        backup.stop();
    }

    private void initTestData() {
        createDefaultUser();
        createDefaultData();
    }

    private void createDefaultData() {
        @NotNull final String defaultUserId = userService.findByLogin("user").getId();
        @NotNull final String defaultAdminId = userService.findByLogin("admin").getId();
        projectService.add(new Project(defaultAdminId, "Project 1", "-"));
        projectService.add(new Project(defaultAdminId, "Project 2", "-"));
        projectService.add(new Project(defaultUserId, "Project 3", "-"));
        projectService.add(new Project(defaultUserId, "Project 4", "-"));
        taskService.add(new Task(defaultAdminId, "Task 1", "Default Task"));
        taskService.add(new Task(defaultAdminId, "Task 2", "Default Task"));
        taskService.add(new Task(defaultUserId, "Task 3", "Default Task"));
        taskService.add(new Task(defaultUserId, "Task 4", "Default Task"));
        taskService.findByName(defaultAdminId, "Task 1").setProjectId(projectService.findByName(defaultAdminId, "Project 1").getId());
        taskService.findByName(defaultAdminId, "Task 2").setProjectId(projectService.findByName(defaultAdminId, "Project 2").getId());
        taskService.findByName(defaultUserId, "Task 3").setProjectId(projectService.findByName(defaultUserId, "Project 3").getId());
        taskService.findByName(defaultUserId, "Task 4").setProjectId(projectService.findByName(defaultUserId, "Project 4").getId());
    }

    private void createDefaultUser() {
        @NotNull final User user = new User("user", HashUtil.encrypt(getPropertyService(), "user"));
        user.setRole(Role.USER);
        user.setEmail("user@folder.com");
        userService.add(user);
        @NotNull final User admin = new User("admin", HashUtil.encrypt(getPropertyService(), "admin"));
        admin.setRole(Role.ADMIN);
        user.setEmail("admin@folder.com");
        userService.add(admin);
    }

    private void displayWelcome() {
        System.out.println("---Welcome to task manager---");
    }

    private void initCommands() {
        @NotNull final Reflections reflections = new Reflections("com.tsc.apogasiy.server.command");
        for (@NotNull final Class<? extends AbstractCommand> clazz : reflections.getSubTypesOf(AbstractCommand.class)) {
            if (!Modifier.isAbstract(clazz.getModifiers())) {
                try {
                    registry(clazz.newInstance());
                } catch (InstantiationException | IllegalAccessException e) {
                    logService.error(e);
                }
            }
        }
    }

    private void runCommand(@Nullable final String command) {
        runCommand(command, true);
    }

    public void runCommand(@Nullable final String command, boolean checkRoles) {
        if (!Optional.ofNullable(command).isPresent() || command.isEmpty())
            return;
        @Nullable AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (!Optional.ofNullable(abstractCommand).isPresent())
            throw new UnknownCommandException(command);
        if (checkRoles) {
            @Nullable final Role[] roles = abstractCommand.roles();
            authService.checkRoles(roles);
        }
        abstractCommand.execute();
    }

    private void registry(@Nullable AbstractCommand command) {
        if (!Optional.ofNullable(command).isPresent()) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

}
