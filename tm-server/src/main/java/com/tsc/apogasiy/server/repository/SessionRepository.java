package com.tsc.apogasiy.server.repository;

import com.tsc.apogasiy.server.api.repository.ISessionRepository;
import com.tsc.apogasiy.server.model.Session;
import org.jetbrains.annotations.NotNull;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

}
