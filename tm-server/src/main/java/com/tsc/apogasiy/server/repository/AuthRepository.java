package com.tsc.apogasiy.server.repository;

import com.tsc.apogasiy.server.api.repository.IAuthRepository;
import org.jetbrains.annotations.Nullable;

public class AuthRepository implements IAuthRepository {

    @Nullable
    private String currentUserId;

    @Override
    @Nullable
    public String getCurrentUserId() {
        return currentUserId;
    }

    @Override
    public void setCurrentUserId(@Nullable final String currentUserId) {
        this.currentUserId = currentUserId;
    }

}
