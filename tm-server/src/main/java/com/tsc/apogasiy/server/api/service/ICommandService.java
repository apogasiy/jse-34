package com.tsc.apogasiy.server.api.service;

import com.tsc.apogasiy.server.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface ICommandService {

    @Nullable
    AbstractCommand getCommandByName(@Nullable final String name);

    @Nullable
    AbstractCommand getCommandByArg(@Nullable final String arg);

    Collection<AbstractCommand> getCommands();

    Collection<AbstractCommand> getArguments();

    Collection<String> getListCommandName();

    Collection<String> getListCommandArg();

    void add(@NotNull final AbstractCommand command);


}
