package com.tsc.apogasiy.server.exception.empty;

import com.tsc.apogasiy.server.exception.AbstractException;

public class EmptyUserIdException extends AbstractException {

    public EmptyUserIdException() {
        super("Error. User id is empty.");
    }

}
