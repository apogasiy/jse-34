package com.tsc.apogasiy.server.exception.entity;

import com.tsc.apogasiy.server.exception.AbstractException;

public class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Error! Task not found!");
    }

}
