package com.tsc.apogasiy.server.exception.empty;

import com.tsc.apogasiy.server.exception.AbstractException;

public class EmptyPasswordException extends AbstractException {

    public EmptyPasswordException() {
        super("Error! Password is empty!");
    }

}
