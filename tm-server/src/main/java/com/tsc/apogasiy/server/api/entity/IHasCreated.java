package com.tsc.apogasiy.server.api.entity;

import org.jetbrains.annotations.Nullable;

import java.util.Date;

public interface IHasCreated {

    @Nullable Date getCreated();

    void setCreated(@Nullable final Date created);

}
