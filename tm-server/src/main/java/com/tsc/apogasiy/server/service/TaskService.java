package com.tsc.apogasiy.server.service;

import com.tsc.apogasiy.server.enumerated.Status;
import com.tsc.apogasiy.server.exception.empty.*;
import com.tsc.apogasiy.server.exception.system.IndexIncorrectException;
import com.tsc.apogasiy.server.api.repository.ITaskRepository;
import com.tsc.apogasiy.server.api.service.ITaskService;
import com.tsc.apogasiy.server.exception.empty.*;
import com.tsc.apogasiy.server.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class TaskService extends AbstractOwnerService<Task> implements ITaskService {

    @NotNull
    private final ITaskRepository taskRepository;

    public TaskService(@NotNull final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(@Nullable final String userId, @Nullable final String name) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty())
            throw new EmptyNameException();
        @NotNull final Task task = new Task(userId, name);
        taskRepository.add(task);
    }

    @Override
    public void create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty())
            throw new EmptyNameException();
        if (description == null || description.isEmpty())
            throw new EmptyDescriptionException();
        @NotNull final Task task = new Task(userId, name, description);
        taskRepository.add(task);
    }

    @Override
    @Nullable
    public Task findByName(@Nullable final String userId, @Nullable final String name) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty())
            throw new EmptyNameException();
        return taskRepository.findByName(userId, name);
    }

    @Override
    @Nullable
    public Task removeByName(@Nullable final String userId, @Nullable final String name) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty())
            throw new EmptyNameException();
        return taskRepository.removeByName(userId, name);
    }

    private void update(@NotNull Task task, @NotNull final String name, @Nullable final String description) {
        task.setName(name);
        task.setDescription(description);
    }

    @Override
    @Nullable
    public Task updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(id).isPresent() || id.isEmpty())
            throw new EmptyIdException();
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty())
            throw new EmptyNameException();
        @NotNull final Task task = taskRepository.findById(userId, id);
        if (Optional.ofNullable(task).isPresent())
            update(task, name, description);
        return task;
    }

    @Override
    @Nullable
    public Task updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        Optional.ofNullable(index).orElseThrow(EmptyIndexException::new);
        if (index < 0)
            throw new IndexIncorrectException();
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty())
            throw new EmptyNameException();
        @NotNull final Task task = taskRepository.findByIndex(userId, index);
        if (Optional.ofNullable(task).isPresent())
            update(task, name, description);
        return task;
    }

    public boolean existsById(@Nullable final String userId, final @Nullable String id) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(id).isPresent() || id.isEmpty())
            throw new EmptyIdException();
        return taskRepository.existsById(userId, id);
    }

    public boolean existsByIndex(@Nullable final String userId, int index) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        return taskRepository.existsByIndex(userId, index);
    }

    @Override
    public boolean existsByName(@Nullable final String userId, final String name) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        return taskRepository.existsByName(userId, name);
    }

    @Override
    @Nullable
    public Task startById(@Nullable final String userId, @Nullable final String id) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(id).isPresent() || id.isEmpty())
            throw new EmptyNameException();
        return taskRepository.startById(userId, id);
    }

    @Override
    @Nullable
    public Task startByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        Optional.ofNullable(index).orElseThrow(EmptyIndexException::new);
        if (index < 0)
            throw new IndexIncorrectException();
        return taskRepository.startByIndex(userId, index);
    }

    @Override
    @Nullable
    public Task startByName(@Nullable final String userId, @Nullable final String name) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty())
            throw new EmptyNameException();
        return taskRepository.startByName(userId, name);
    }

    @Override
    @Nullable
    public Task finishById(@Nullable final String userId, @Nullable final String id) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(id).isPresent() || id.isEmpty())
            throw new EmptyIdException();
        return taskRepository.finishById(userId, id);
    }

    @Override
    @Nullable
    public Task finishByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        Optional.ofNullable(index).orElseThrow(EmptyIndexException::new);
        if (index < 0)
            throw new IndexIncorrectException();
        return taskRepository.finishByIndex(userId, index);
    }

    @Override
    @Nullable
    public Task finishByName(@Nullable final String userId, @Nullable final String name) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty())
            throw new EmptyNameException();
        return taskRepository.finishByName(userId, name);
    }

    @Override
    @Nullable
    public Task changeStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(id).isPresent() || id.isEmpty())
            throw new EmptyIdException();
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        return taskRepository.changeStatusById(userId, id, status);
    }

    @Override
    @Nullable
    public Task changeStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        Optional.ofNullable(index).orElseThrow(EmptyIndexException::new);
        if (index < 0)
            throw new IndexIncorrectException();
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        return taskRepository.changeStatusByIndex(userId, index, status);
    }

    @Override
    @Nullable
    public Task changeStatusByName(@Nullable final String userId, @Nullable final String name, @Nullable final Status status) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty())
            throw new EmptyNameException();
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        return taskRepository.changeStatusByName(userId, name, status);
    }

}
