package com.tsc.apogasiy.server.endpoint;

import com.tsc.apogasiy.server.api.service.IPropertyService;
import com.tsc.apogasiy.server.api.service.IServiceLocator;
import com.tsc.apogasiy.server.enumerated.Role;
import com.tsc.apogasiy.server.model.Session;
import com.tsc.apogasiy.server.model.User;
import com.tsc.apogasiy.server.service.UserService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class UserAdminEndpoint extends AbstractEndpoint{

    public UserAdminEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    public void clearUser(@NotNull @WebParam(name = "session") final Session session) {
        validateAdminSession(session);
        serviceLocator.getUserService().clear();
    }

    @WebMethod
    public List<User> findAllUser(@NotNull final Session session) {
        validateAdminSession(session);
        return serviceLocator.getUserService().findAll();
    }

    @WebMethod
    public void addUser(@NotNull @WebParam(name = "session") final Session session, @NotNull @WebParam(name = "user") final User user) {
        validateAdminSession(session);
        serviceLocator.getUserService().add(user);
    }

    @WebMethod
    public void addAllUser(@NotNull @WebParam(name = "session") final Session session, @NotNull @WebParam(name = "users", partName = "user") final List<User> users) {
        validateAdminSession(session);
        serviceLocator.getUserService().addAll(users);
    }

    @WebMethod
    public @NotNull User createUser(@NotNull final @WebParam(name = "session") Session session, @NotNull @WebParam(name = "login") final String login, @Nullable final @WebParam(name = "password") String password, @NotNull @WebParam(name = "role") final Role role) {
        validateAdminSession(session);
        return serviceLocator.getUserService().create(login, password, role);
    }

    @WebMethod
    public @NotNull User setPasswordUser(@NotNull @WebParam(name = "session") final Session session, @Nullable @WebParam(name = "userId") final String userId, @Nullable @WebParam(name = "password") final String password) {
        validateAdminSession(session);
        return serviceLocator.getUserService().setPassword(userId, password);
    }

    @WebMethod
    public @NotNull User setRoleUser(@NotNull @WebParam(name = "session") final Session session, @Nullable @WebParam(name = "userId") final String userId, @Nullable @WebParam(name = "role") final Role role) {
        validateAdminSession(session);
        return serviceLocator.getUserService().setRole(userId, role);
    }

    @WebMethod
    public @Nullable User findByIdUser(@NotNull @WebParam(name = "session") final Session session, @Nullable @WebParam(name = "id") final String id) {
        validateAdminSession(session);
        return serviceLocator.getUserService().findById(id);
    }

    @WebMethod
    public @NotNull User findByLoginUser(@NotNull @WebParam(name = "session") final Session session, @Nullable @WebParam(name = "login") final String login) {
        validateAdminSession(session);
        return serviceLocator.getUserService().findByLogin(login);
    }

    @WebMethod
    public @Nullable User findByEmailUser(@NotNull @WebParam(name = "session") final Session session, @Nullable @WebParam(name = "email") final String email) {
        validateAdminSession(session);
        return serviceLocator.getUserService().findByEmail(email);
    }

    @WebMethod
    public @Nullable User removeByIdUser(@NotNull @WebParam(name = "session") final Session session, @Nullable @WebParam(name = "id") final String id) {
        validateAdminSession(session);
        return serviceLocator.getUserService().removeById(id);
    }

    @WebMethod
    public @Nullable User removeByLoginUser(@NotNull @WebParam(name = "session") final Session session, @Nullable @WebParam(name = "login") final String login) {
        validateAdminSession(session);
        return serviceLocator.getUserService().removeByLogin(login);
    }

    @WebMethod
    public boolean isLoginExistsUser(@NotNull @WebParam(name = "session") final Session session, @NotNull @WebParam(name = "login") final String login) {
        validateAdminSession(session);
        return serviceLocator.getUserService().isLoginExists(login);
    }

    @WebMethod
    public boolean isEmailExistsUser(@NotNull @WebParam(name = "session") final Session session, @NotNull @WebParam(name = "email") final String email) {
        validateAdminSession(session);
        return serviceLocator.getUserService().isEmailExists(email);
    }

    @WebMethod
    public @NotNull User updateByIdUser(@NotNull @WebParam(name = "session") final Session session, @Nullable @WebParam(name = "id") final String id, @Nullable @WebParam(name = "lastName") final String lastName, @Nullable @WebParam(name = "firstName") final String firstName, @Nullable @WebParam(name = "middleName") final String middleName, @Nullable @WebParam(name = "email") final String email) {
        validateAdminSession(session);
        return serviceLocator.getUserService().updateById(id, lastName, firstName, middleName, email);
    }

    @WebMethod
    public @NotNull User updateByLoginUser(@NotNull @WebParam(name = "session") final Session session, @Nullable @WebParam(name = "login") final String login, @Nullable @WebParam(name = "lastName") final String lastName, @Nullable @WebParam(name = "firstName") final String firstName, @Nullable @WebParam(name = "middleName") final String middleName, @Nullable @WebParam(name = "email") final String email) {
        validateAdminSession(session);
        return serviceLocator.getUserService().updateByLogin(login, lastName, firstName, middleName, email);
    }

    @WebMethod
    public @NotNull User lockUserByLoginUser(@NotNull @WebParam(name = "session") final Session session, @Nullable @WebParam(name = "login") final String login) {
        validateAdminSession(session);
        return serviceLocator.getUserService().lockUserByLogin(login);
    }

    @WebMethod
    public @NotNull User unlockUserByLoginUser(@NotNull @WebParam(name = "session") final Session session, @Nullable @WebParam(name = "login") final String login) {
        validateAdminSession(session);
        return serviceLocator.getUserService().unlockUserByLogin(login);
    }

}
