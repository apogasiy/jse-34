package com.tsc.apogasiy.server.service;

import com.tsc.apogasiy.server.api.service.ILogService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.util.Optional;
import java.util.logging.*;

public class LogService implements ILogService {

    @NotNull
    private static final String FILE_NAME = "logger.properties";

    @NotNull
    private static final String COMMANDS = "COMMANDS";

    @NotNull
    private static final String COMMANDS_FILE = "./commands.txt";

    @NotNull
    private static final String MESSAGES = "MESSAGES";

    @NotNull
    private static final String MESSAGES_FILE = "./messages.txt";

    @NotNull
    private static final String ERRORS = "ERRORS";

    @NotNull
    private static final String ERRORS_FILE = "./errors.txt";

    @NotNull
    private final ConsoleHandler consoleHandler = getConsoleHandler();

    @NotNull
    private final LogManager manager = LogManager.getLogManager();

    @NotNull
    private final Logger root = Logger.getLogger("");

    @NotNull
    private final Logger commands = Logger.getLogger(COMMANDS);

    @NotNull
    private final Logger errors = Logger.getLogger(ERRORS);

    @NotNull
    private final Logger messages = Logger.getLogger(MESSAGES);

    {
        init();
        registry(commands, COMMANDS_FILE, false);
        registry(errors, ERRORS_FILE, true);
        registry(messages, MESSAGES_FILE, true);
    }

    private ConsoleHandler getConsoleHandler() {
        @NotNull final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    private void init() {
        try {
            manager.readConfiguration(this.getClass().getClassLoader().getResourceAsStream(FILE_NAME));
        } catch (IOException e) {
            root.severe(e.getMessage());
        }
    }

    private void registry(@NotNull final Logger logger, @NotNull final String fileName, final boolean isConsole) {
        try {
            if (isConsole)
                logger.addHandler(consoleHandler);
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(fileName));
        } catch (final IOException e) {
            root.severe(e.getMessage());
        }
    }


    @Override
    public void info(@Nullable final String message) {
        if (!Optional.ofNullable(message).isPresent() || message.isEmpty())
            return;
        messages.info(message);
    }

    @Override
    public void debug(@Nullable final String message) {
        if (!Optional.ofNullable(message).isPresent() || message.isEmpty())
            return;
        messages.info(message);
    }

    @Override
    public void command(@Nullable final String message) {
        if (!Optional.ofNullable(message).isPresent() || message.isEmpty())
            return;
        commands.fine(message);
    }

    @Override
    public void error(@Nullable final Exception e) {
        if (e == null)
            return;
        errors.log(Level.SEVERE, e.getMessage(), e);
    }

}
