package com.tsc.apogasiy.server.endpoint;


import com.tsc.apogasiy.server.api.service.IServiceLocator;
import com.tsc.apogasiy.server.enumerated.Status;
import com.tsc.apogasiy.server.exception.entity.TaskNotFoundException;
import com.tsc.apogasiy.server.model.Session;
import com.tsc.apogasiy.server.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
import java.util.Optional;

@WebService
public class TaskEndpoint extends AbstractEndpoint{

    public TaskEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    public void addAllTask(@Nullable @WebParam(name = "session") Session session, @Nullable List<Task> entities) {
        validateSession(session);
        serviceLocator.getTaskService().addAll(entities);
    }

    @WebMethod
    public @NotNull List<Task> findAllTask(@Nullable @WebParam(name = "session") Session session) {
        validateSession(session);
        return serviceLocator.getTaskService().findAll(session.getUserId());
    }

    @WebMethod
    public @Nullable Task removeByIdTask(@Nullable @WebParam(name = "session") Session session, @NotNull @WebParam(name = "id") String id) {
        validateSession(session);
        final Task task = Optional.ofNullable(serviceLocator.getTaskService().findById(session.getUserId(), id)).orElseThrow(TaskNotFoundException::new);
        return serviceLocator.getTaskService().removeById(task.getId());
    }

    @WebMethod
    public @NotNull List<Task> findAllByUserTask(@Nullable @WebParam(name = "session") Session session) {
        validateSession(session);
        return serviceLocator.getTaskService().findAll(session.getUserId());
    }

    @WebMethod
    public @Nullable Task findByIdTask(@Nullable @WebParam(name = "session") Session session, @WebParam(name = "id") @Nullable String id) {
        validateSession(session);
        return serviceLocator.getTaskService().findById(session.getUserId(), id);
    }

    @WebMethod
    public void clearTask(@Nullable @WebParam(name = "session") Session session) {
        validateSession(session);
        serviceLocator.getTaskService().clear(session.getUserId());
    }

    @WebMethod
    public void removeTask(@Nullable @WebParam(name = "session") Session session, @WebParam(name = "task") Task entity) {
        validateSession(session);
        serviceLocator.getTaskService().remove(session.getUserId(), entity);
    }

    @WebMethod
    public @NotNull Integer getSizeTask(@Nullable @WebParam(name = "session") Session session) {
        validateSession(session);
        return serviceLocator.getTaskService().getSize(session.getUserId());
    }

    @WebMethod
    public void createTask(@Nullable @WebParam(name = "session") Session session, @Nullable @WebParam(name = "name") String name, @Nullable @WebParam(name = "description") String description) {
        validateSession(session);
        if (Optional.ofNullable(description).isPresent())
            serviceLocator.getTaskService().create(session.getUserId(), name, description);
        else
            serviceLocator.getTaskService().create(session.getUserId(), name);
    }

    @WebMethod
    public @Nullable Task findByNameTask(@Nullable @WebParam(name = "session") Session session, @Nullable @WebParam(name = "name") String name) {
        validateSession(session);
        return serviceLocator.getTaskService().findByName(session.getUserId(), name);
    }

    @WebMethod
    public @Nullable Task findByIndexTask(@Nullable @WebParam(name = "session") Session session, @WebParam(name = "index") @Nullable Integer index) {
        validateSession(session);
        return serviceLocator.getTaskService().findByIndex(session.getUserId(), index);
    }

    @WebMethod
    public @Nullable Task removeByNameTask(@Nullable @WebParam(name = "session") Session session, @Nullable @WebParam(name = "name") String name) {
        validateSession(session);
        return serviceLocator.getTaskService().removeByName(session.getUserId(), name);
    }

    @WebMethod
    public @Nullable Task updateByIdTask(@Nullable @WebParam(name = "session") Session session, @Nullable @WebParam(name = "id") String id, @Nullable @WebParam(name = "name") String name, @Nullable @WebParam(name = "description") String description) {
        validateSession(session);
        return serviceLocator.getTaskService().updateById(session.getUserId(), id, name, description);
    }

    @WebMethod
    public @Nullable Task updateByIndexTask(@Nullable @WebParam(name = "session") Session session, @Nullable @WebParam(name = "index") Integer index, @Nullable @WebParam(name = "name") String name, @Nullable @WebParam(name = "description") String description) {
        validateSession(session);
        return serviceLocator.getTaskService().updateByIndex(session.getUserId(), index, name, description);
    }

    @WebMethod
    public boolean existsByNameTask(@Nullable @WebParam(name = "session") Session session, @Nullable @WebParam(name = "name") String name) {
        validateSession(session);
        return serviceLocator.getTaskService().existsByName(session.getUserId(), name);
    }

    @WebMethod
    public @Nullable Task startByIdTask(@Nullable @WebParam(name = "session") Session session, @Nullable @WebParam(name = "id") String id) {
        validateSession(session);
        return serviceLocator.getTaskService().startById(session.getUserId(), id);
    }

    @WebMethod
    public @Nullable Task startByIndexTask(@Nullable @WebParam(name = "session") Session session, @Nullable @WebParam(name = "index") Integer index) {
        validateSession(session);
        return serviceLocator.getTaskService().startByIndex(session.getUserId(), index);
    }

    @WebMethod
    public @Nullable Task startByNameTask(@Nullable @WebParam(name = "session") Session session, @Nullable @WebParam(name = "name") String name) {
        validateSession(session);
        return serviceLocator.getTaskService().startByName(session.getUserId(), name);
    }

    @WebMethod
    public @Nullable Task finishByIdTask(@Nullable @WebParam(name = "session") Session session, @Nullable @WebParam(name = "id") String id) {
        validateSession(session);
        return serviceLocator.getTaskService().finishById(session.getUserId(), id);
    }

    @WebMethod
    public @Nullable Task finishByIndexTask(@Nullable @WebParam(name = "session") Session session, @Nullable @WebParam(name = "index") Integer index) {
        validateSession(session);
        return serviceLocator.getTaskService().finishByIndex(session.getUserId(), index);
    }

    @WebMethod
    public @Nullable Task finishByNameTask(@Nullable @WebParam(name = "session") Session session, @Nullable @WebParam(name = "name") String name) {
        validateSession(session);
        return serviceLocator.getTaskService().finishByName(session.getUserId(), name);
    }

    @WebMethod
    public @Nullable Task changeStatusByIdTask(@Nullable @WebParam(name = "session") Session session, @Nullable @WebParam(name = "id") String id, @Nullable @WebParam(name = "status") Status status) {
        validateSession(session);
        return serviceLocator.getTaskService().changeStatusById(session.getUserId(), id, status);
    }

    @WebMethod
    public @Nullable Task changeStatusByIndexTask(@Nullable @WebParam(name = "session") Session session, @Nullable @WebParam(name = "index") Integer index, @Nullable @WebParam(name = "status") Status status) {
        validateSession(session);
        return serviceLocator.getTaskService().changeStatusByIndex(session.getUserId(), index, status);
    }

    @WebMethod
    public @Nullable Task changeStatusByNameTask(@Nullable @WebParam(name = "session") Session session, @Nullable @WebParam(name = "name") String name, @Nullable @WebParam(name = "status") Status status) {
        validateSession(session);
        return serviceLocator.getTaskService().changeStatusByName(session.getUserId(), name, status);
    }

    @WebMethod
    public @Nullable List<Task> findTaskByProjectIdTask(@Nullable @WebParam(name = "session") Session session, @Nullable @WebParam(name = "projectId") String projectId) {
        validateSession(session);
        return serviceLocator.getProjectTaskService().findTaskByProjectId(session.getUserId(), projectId);
    }

    @WebMethod
    public @Nullable Task bindTaskByIdTask(@Nullable @WebParam(name = "session") Session session, @Nullable @WebParam(name = "projectId") String projectId, @Nullable @WebParam(name = "taskId") String taskId) {
        validateSession(session);
        return serviceLocator.getProjectTaskService().bindTaskById(session.getUserId(), projectId, taskId);
    }

    @WebMethod
    public @Nullable Task unbindTaskByIdTask(@Nullable @WebParam(name = "session") Session session, @Nullable @WebParam(name = "projectId") String projectId, @Nullable @WebParam(name = "taskId") String taskId) {
        validateSession(session);
        return serviceLocator.getProjectTaskService().unbindTaskById(session.getUserId(), projectId, taskId);
    }

}
