package com.tsc.apogasiy.server.endpoint;

import com.tsc.apogasiy.server.api.service.IServiceLocator;
import com.tsc.apogasiy.server.enumerated.Status;
import com.tsc.apogasiy.server.model.Project;
import com.tsc.apogasiy.server.model.Session;
import com.tsc.apogasiy.server.service.ProjectService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectEndpoint extends AbstractEndpoint{

    public ProjectEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    public void addAllProject(@Nullable @WebParam(name = "session") Session session, @Nullable @WebParam(name = "projects", partName = "project") List<Project> entities) {
        validateSession(session);
        serviceLocator.getProjectService().addAll(entities);
    }

    @WebMethod
    public @Nullable Project removeByIdProject(@Nullable @WebParam(name = "session") Session session, @NotNull @WebParam(name = "projectId") String id) {
        validateSession(session);
        return serviceLocator.getProjectTaskService().removeById(session.getUserId(), id);
    }

    @WebMethod
    public @Nullable Project removeByIndexProject(@Nullable @WebParam(name = "session") Session session, @NotNull @WebParam(name = "index") Integer index) {
        validateSession(session);
        return serviceLocator.getProjectTaskService().removeByIndex(session.getUserId(), index);
    }

    @WebMethod
    public @Nullable Project removeByNameProject(@Nullable @WebParam(name = "session") Session session, @NotNull @WebParam(name = "name") String name) {
        validateSession(session);
        return serviceLocator.getProjectTaskService().removeByName(session.getUserId(), name);
    }

    @WebMethod
    public @NotNull List<Project> findAllProject(@Nullable @WebParam(name = "session") Session session) {
        validateSession(session);
        return serviceLocator.getProjectService().findAll(session.getUserId());
    }

    @WebMethod
    public @Nullable Project findByIdProject(@Nullable @WebParam(name = "session") Session session, @Nullable @WebParam(name = "projectId") String id) {
        validateSession(session);
        return serviceLocator.getProjectService().findById(session.getUserId(), id);
    }

    @WebMethod
    public void clearUserProject(@Nullable @WebParam(name = "session") Session session) {
        validateSession(session);
        serviceLocator.getProjectService().clear(session.getUserId());
    }

    @WebMethod
    public void removeUserProject(@Nullable @WebParam(name = "session") Session session, @WebParam(name = "project") Project entity) {
        validateSession(session);
        serviceLocator.getProjectService().remove(session.getUserId(), entity);
    }

    @WebMethod
    public @NotNull Integer getSizeProject(@Nullable @WebParam(name = "session") Session session) {
        validateSession(session);
        return serviceLocator.getProjectService().getSize(session.getUserId());
    }

    @WebMethod
    public void createProject(@Nullable @WebParam(name = "session") Session session, @WebParam(name = "name") @Nullable String name, @Nullable @WebParam(name = "description") String description) {
        validateSession(session);
        serviceLocator.getProjectService().create(session.getUserId(), name, description);
    }

    @WebMethod
    public @Nullable Project findByNameProject(@Nullable @WebParam(name = "session") Session session, @Nullable @WebParam(name = "name") String name) {
        validateSession(session);
        return serviceLocator.getProjectService().findByName(session.getUserId(), name);
    }

    @WebMethod
    public @Nullable Project findByIndexProject(@Nullable @WebParam(name = "session") Session session, @Nullable @WebParam(name = "index") Integer index) {
        validateSession(session);
        return serviceLocator.getProjectService().findByIndex(session.getUserId(), index);
    }

    @WebMethod
    public @Nullable Project updateByIdProject(@Nullable @WebParam(name = "session") Session session, @Nullable @WebParam(name = "id") String id, @Nullable @WebParam(name = "name") String name, @Nullable @WebParam(name = "description") String description) {
        validateSession(session);
        return serviceLocator.getProjectService().updateById(session.getUserId(), id, name, description);
    }

    @WebMethod
    public @Nullable Project updateByIndexProject(@Nullable @WebParam(name = "session") Session session, @Nullable @WebParam(name = "index") Integer index, @Nullable @WebParam(name = "name") String name, @Nullable @WebParam(name = "description") String description) {
        validateSession(session);
        return serviceLocator.getProjectService().updateByIndex(session.getUserId(), index, name, description);
    }

    @WebMethod
    public boolean existsByIndexProject(@Nullable @WebParam(name = "session") Session session, @NotNull @WebParam(name = "index") Integer index) {
        validateSession(session);
        return serviceLocator.getProjectService().existsByIndex(session.getUserId(), index);
    }

    @WebMethod
    public boolean existsByNameProject(@Nullable @WebParam(name = "session") Session session, @NotNull @WebParam(name = "name") String name) {
        validateSession(session);
        return serviceLocator.getProjectService().existsByName(session.getUserId(), name);
    }

    @WebMethod
    public @Nullable Project startByIdProject(@Nullable @WebParam(name = "session") Session session, @Nullable @WebParam(name = "id") String id) {
        validateSession(session);
        return serviceLocator.getProjectService().startById(session.getUserId(), id);
    }

    @WebMethod
    public @Nullable Project startByIndexProject(@Nullable @WebParam(name = "session") Session session, @Nullable @WebParam(name = "index") Integer index) {
        validateSession(session);
        return serviceLocator.getProjectService().startByIndex(session.getUserId(), index);
    }

    @WebMethod
    public @Nullable Project startByNameProject(@Nullable @WebParam(name = "session") Session session, @Nullable @WebParam(name = "name") String name) {
        validateSession(session);
        return serviceLocator.getProjectService().startByName(session.getUserId(), name);
    }

    @WebMethod
    public @Nullable Project finishByIdProject(@Nullable @WebParam(name = "session") Session session, @Nullable @WebParam(name = "id") String id) {
        validateSession(session);
        return serviceLocator.getProjectService().finishById(session.getUserId(), id);
    }

    @WebMethod
    public @Nullable Project finishByIndexProject(@Nullable @WebParam(name = "session") Session session, @Nullable @WebParam(name = "index") Integer index) {
        validateSession(session);
        return serviceLocator.getProjectService().finishByIndex(session.getUserId(), index);
    }

    @WebMethod
    public @Nullable Project finishByNameProject(@Nullable @WebParam(name = "session") Session session, @Nullable @WebParam(name = "name") String name) {
        validateSession(session);
        return serviceLocator.getProjectService().finishByName(session.getUserId(), name);
    }

    @WebMethod
    public @Nullable Project changeStatusByIdProject(@Nullable @WebParam(name = "session") Session session, @Nullable @WebParam(name = "id") String id, @Nullable @WebParam(name = "status") Status status) {
        validateSession(session);
        return serviceLocator.getProjectService().changeStatusById(session.getUserId(), id, status);
    }

    @WebMethod
    public @Nullable Project changeStatusByIndexProject(@Nullable @WebParam(name = "session") Session session, @Nullable @WebParam(name = "index") Integer index, @Nullable @WebParam(name = "status") Status status) {
        validateSession(session);
        return serviceLocator.getProjectService().changeStatusByIndex(session.getUserId(), index, status);
    }

    @WebMethod
    public @Nullable Project changeStatusByNameProject(@Nullable @WebParam(name = "session") Session session, @Nullable @WebParam(name = "name") String name, @Nullable @WebParam(name = "status") Status status) {
        validateSession(session);
        return serviceLocator.getProjectService().changeStatusByName(session.getUserId(), name, status);
    }

}