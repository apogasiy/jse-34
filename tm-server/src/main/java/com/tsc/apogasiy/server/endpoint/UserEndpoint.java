package com.tsc.apogasiy.server.endpoint;

import com.tsc.apogasiy.server.api.service.IServiceLocator;
import com.tsc.apogasiy.server.model.Session;
import com.tsc.apogasiy.server.model.User;
import com.tsc.apogasiy.server.service.UserService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class UserEndpoint extends AbstractEndpoint{

    public UserEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    public @NotNull User setPasswordMyUser(@NotNull @WebParam(name = "session") final Session session, @Nullable @WebParam(name = "password") final String password) {
        validateSession(session);
        return serviceLocator.getUserService().setPassword(session.getUserId(), password);
    }

    @WebMethod
    public @NotNull User updateMyUser(@NotNull @WebParam(name = "session") final Session session, @Nullable @WebParam(name = "lastName") final String lastName, @Nullable @WebParam(name = "firstName") final String firstName, @Nullable @WebParam(name = "middleName") final String middleName, @Nullable @WebParam(name = "email") final String email) {
        validateSession(session);
        return serviceLocator.getUserService().updateById(session.getUserId(), lastName, firstName, middleName, email);
    }

}
