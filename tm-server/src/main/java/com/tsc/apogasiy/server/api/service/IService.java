package com.tsc.apogasiy.server.api.service;

import com.tsc.apogasiy.server.model.AbstractEntity;
import com.tsc.apogasiy.server.api.repository.IRepository;

public interface IService<E extends AbstractEntity> extends IRepository<E> {

}