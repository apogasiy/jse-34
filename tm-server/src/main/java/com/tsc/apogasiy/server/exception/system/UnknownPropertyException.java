package com.tsc.apogasiy.server.exception.system;

import com.tsc.apogasiy.server.exception.AbstractException;

public class UnknownPropertyException extends AbstractException {

    public UnknownPropertyException() {
        super("Error! Unknown property!");
    }

}
