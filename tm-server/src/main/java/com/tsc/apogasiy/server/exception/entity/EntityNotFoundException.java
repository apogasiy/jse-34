package com.tsc.apogasiy.server.exception.entity;

import com.tsc.apogasiy.server.exception.AbstractException;

public class EntityNotFoundException extends AbstractException {

    public EntityNotFoundException() {
        super("Error. Entity not found.");
    }

}
