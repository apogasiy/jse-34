package com.tsc.apogasiy.server.enumerated;

public enum Role {

    ADMIN("Administrator"),
    USER("User");

    private final String displayName;

    Role(String displayName) {
        this.displayName = displayName;
    }

    public java.lang.String getDisplayName() {
        return displayName;
    }

}
