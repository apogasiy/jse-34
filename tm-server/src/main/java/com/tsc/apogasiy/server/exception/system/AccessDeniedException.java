package com.tsc.apogasiy.server.exception.system;

import com.tsc.apogasiy.server.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! Access denied!");
    }

}
