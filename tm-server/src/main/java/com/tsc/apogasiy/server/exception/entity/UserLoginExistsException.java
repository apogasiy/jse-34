package com.tsc.apogasiy.server.exception.entity;

import com.tsc.apogasiy.server.exception.AbstractException;
import org.jetbrains.annotations.NotNull;

public class UserLoginExistsException extends AbstractException {

    public UserLoginExistsException(@NotNull final String login) {
        super("Error! User with login '" + login + "' already exists.");
    }

}
