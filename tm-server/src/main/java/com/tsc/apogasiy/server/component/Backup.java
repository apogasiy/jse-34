package com.tsc.apogasiy.server.component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationModule;
import com.tsc.apogasiy.server.dto.Domain;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Backup implements Runnable {

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final ScheduledExecutorService executorService;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.executorService = Executors.newSingleThreadScheduledExecutor();
    }

    @Override
    public void run() {
        save();
    }

    public void start() {
        executorService.scheduleWithFixedDelay(this, 0, bootstrap.getPropertyService().getAutosaveInterval(), TimeUnit.SECONDS);
    }

    public void stop() {
        executorService.shutdown();
    }

    @SneakyThrows
    public void load() {
        @NotNull final String xml = new String(Files.readAllBytes(Paths.get(bootstrap.getPropertyService().getDTOXmlFileName())));
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        bootstrap.getDomainService().setDomain(objectMapper.readValue(xml, Domain.class));
    }

    @SneakyThrows
    public void save() {
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        objectMapper.registerModule(new JaxbAnnotationModule());    // Поддержка аннотаций JAXB
        objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"));
        @NotNull final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(bootstrap.getDomainService().getDomain());
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(bootstrap.getPropertyService().getDTOXmlFileName());
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.close();
    }

}
