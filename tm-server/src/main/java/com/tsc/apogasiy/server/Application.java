package com.tsc.apogasiy.server;

import com.tsc.apogasiy.server.component.Bootstrap;
import org.jetbrains.annotations.NotNull;

public class Application {

    public static void main(final String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
        System.exit(0);
    }

}