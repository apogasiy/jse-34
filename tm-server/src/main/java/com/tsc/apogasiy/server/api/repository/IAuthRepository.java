package com.tsc.apogasiy.server.api.repository;

import org.jetbrains.annotations.Nullable;

public interface IAuthRepository {

    @Nullable String getCurrentUserId();

    void setCurrentUserId(@Nullable final String currentUserId);

}
