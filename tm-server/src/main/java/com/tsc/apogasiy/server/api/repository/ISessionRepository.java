package com.tsc.apogasiy.server.api.repository;

import com.tsc.apogasiy.server.model.Session;
import org.jetbrains.annotations.NotNull;

public interface ISessionRepository extends IRepository<Session> {

}
