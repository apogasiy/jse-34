package com.tsc.apogasiy.server.api.service;

import com.tsc.apogasiy.server.dto.Domain;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IDomainService {

    @NotNull
    public Domain getDomain();

    public void setDomain(@Nullable final Domain domain);

}
