package com.tsc.apogasiy.server.exception.system;

import com.tsc.apogasiy.server.exception.AbstractException;
import org.jetbrains.annotations.NotNull;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException(@NotNull final String command) {
        super("Incorrect command:'" + command + "'");
    }

}
