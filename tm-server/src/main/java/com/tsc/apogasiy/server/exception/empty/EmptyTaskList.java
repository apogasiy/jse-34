package com.tsc.apogasiy.server.exception.empty;

import com.tsc.apogasiy.server.exception.AbstractException;

public class EmptyTaskList extends AbstractException {

    public EmptyTaskList() {
        super("Error! Task list is empty!");
    }

}
