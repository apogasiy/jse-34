package com.tsc.apogasiy.server.exception.empty;

import com.tsc.apogasiy.server.exception.AbstractException;

public class EmptyEmailException extends AbstractException {

    public EmptyEmailException() {
        super("Error! Email is empty!");
    }

}
