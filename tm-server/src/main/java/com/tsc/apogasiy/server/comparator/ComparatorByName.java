package com.tsc.apogasiy.server.comparator;

import com.tsc.apogasiy.server.api.entity.IHasName;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;

public class ComparatorByName implements Comparator<IHasName> {

    private static final ComparatorByName INSTANCE = new ComparatorByName();

    private ComparatorByName() {
    }

    public static ComparatorByName getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(@Nullable final IHasName o1, @Nullable final IHasName o2) {
        if (o1 == null || o2 == null)
            return 0;
        return o1.getName().compareTo(o2.getName());
    }

}
