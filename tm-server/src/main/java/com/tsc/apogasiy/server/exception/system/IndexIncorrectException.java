package com.tsc.apogasiy.server.exception.system;

import com.tsc.apogasiy.server.exception.AbstractException;

public class IndexIncorrectException extends AbstractException {

    public IndexIncorrectException() {
        super("Error! Index is incorrect!");
    }

}
