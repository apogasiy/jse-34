package com.tsc.apogasiy.server.exception.empty;

import com.tsc.apogasiy.server.exception.AbstractException;

public class EmptyProjectList extends AbstractException {

    public EmptyProjectList() {
        super("Error! Project list is empty!");
    }

}
