package com.tsc.apogasiy.server.exception.empty;

import com.tsc.apogasiy.server.exception.AbstractException;

public class EmptyStatusException extends AbstractException {

    public EmptyStatusException() {
        super("Error! Status is empty!");
    }

}
