package com.tsc.apogasiy.server.api.entity;

import com.tsc.apogasiy.server.enumerated.Status;
import org.jetbrains.annotations.Nullable;

public interface IHasStatus {

    @Nullable Status getStatus();

    void setStatus(@Nullable final Status status);

}
