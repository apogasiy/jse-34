package com.tsc.apogasiy.server.exception.empty;

import com.tsc.apogasiy.server.exception.AbstractException;

public class EmptyLastNameException extends AbstractException {

    public EmptyLastNameException() {
        super("Error! Last name is empty!");
    }

}
