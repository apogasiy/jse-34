package com.tsc.apogasiy.server.exception.entity;

import com.tsc.apogasiy.server.exception.AbstractException;
import org.jetbrains.annotations.NotNull;

public class UserEmailExistsException extends AbstractException {

    public UserEmailExistsException(@NotNull final String email) {
        super("Error! User with email '" + email + "' already exists.");
    }

}
