package com.tsc.apogasiy.server.endpoint;

import com.tsc.apogasiy.server.api.service.IServiceLocator;
import com.tsc.apogasiy.server.model.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class SessionEndpoint extends AbstractEndpoint{

    public SessionEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    public @NotNull Session open(@NotNull @WebParam(name = "login") String login, @NotNull @WebParam(name = "password") String password) {
        return serviceLocator.getSessionService().open(login, password);
    }

    @WebMethod
    public @Nullable Session close(@NotNull @WebParam(name = "session") Session session) {
        return serviceLocator.getSessionService().close(session);
    }

}
