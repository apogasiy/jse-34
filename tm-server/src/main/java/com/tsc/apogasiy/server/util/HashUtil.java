package com.tsc.apogasiy.server.util;

import com.tsc.apogasiy.server.api.service.IPropertyService;
import org.jetbrains.annotations.NotNull;

public interface HashUtil {

    @NotNull
    static String encrypt(@NotNull final IPropertyService propertyService, @NotNull final String value) {
        @NotNull String result = value;
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        @NotNull final String secret = propertyService.getPasswordSecret();
        for (int i = 0; i < iteration; i++) {
            result = md5(secret + result + secret);
        }
        return result;
    }

    @NotNull
    static String md5(@NotNull final String value) {
        try {
            @NotNull java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            @NotNull final byte[] array = md.digest(value.getBytes());
            @NotNull final StringBuffer sb = new StringBuffer();
            for (byte b : array) {
                sb.append(Integer.toHexString((b & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

}
