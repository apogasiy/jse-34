package com.tsc.apogasiy.server.exception.entity;

import com.tsc.apogasiy.server.exception.AbstractException;

public class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Error! Project was not found!");
    }

}
