package com.tsc.apogasiy.server.api.service;

import org.jetbrains.annotations.NotNull;

public interface IHashService {

    @NotNull String getPasswordSecret();

    @NotNull Integer getPasswordIteration();

}
