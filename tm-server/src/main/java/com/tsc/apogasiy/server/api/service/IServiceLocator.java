package com.tsc.apogasiy.server.api.service;

import org.jetbrains.annotations.Nullable;

public interface IServiceLocator {

    @Nullable ITaskService getTaskService();

    @Nullable IProjectService getProjectService();

    @Nullable IProjectTaskService getProjectTaskService();

    @Nullable IUserService getUserService();

    @Nullable IAuthService getAuthService();

    @Nullable IPropertyService getPropertyService();

    @Nullable ISessionService getSessionService();

}
