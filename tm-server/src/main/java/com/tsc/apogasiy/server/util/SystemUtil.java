package com.tsc.apogasiy.server.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.management.ManagementFactory;
import java.util.Optional;

public class SystemUtil {

    @NotNull
    public static long getPID() {
        @Nullable final String processName = ManagementFactory.getRuntimeMXBean().getName();
        if (!Optional.ofNullable(processName).isPresent() || processName.isEmpty())
            return 0;
        else
            return Long.parseLong(processName.split("@")[0]);
    }

}
