package com.tsc.apogasiy.server.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class Session extends AbstractOwnerEntity implements Cloneable{

    @NotNull
    private Long timestamp;

    @NotNull
    private String signature;

    @Override
    @SneakyThrows
    public Session clone() {
        return (Session) super.clone();
    }

    public void resetSignature() {
        setSignature("");
    }

    public boolean isReqAttrFilled() {
        if (getId().isEmpty() || getUserId().isEmpty() || timestamp == null || getSignature().isEmpty())
            return false;
        else
            return true;
    }

}
