package com.tsc.apogasiy.server.exception.empty;

import com.tsc.apogasiy.server.exception.AbstractException;

public class EmptyUserListException extends AbstractException {

    public EmptyUserListException() {
        super("Error! User list is empty!");
    }

}
