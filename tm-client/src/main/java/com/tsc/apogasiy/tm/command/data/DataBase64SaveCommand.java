package com.tsc.apogasiy.tm.command.data;

import com.tsc.apogasiy.tm.command.AbstractDataCommand;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.util.Base64;

public class DataBase64SaveCommand extends AbstractDataCommand {

    @Override
    public @NotNull String getCommand() {
        return "data-save-base64";
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return "Save state to base64";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final File file = new File(serviceLocator.getPropertyService().getDTOBase64FileName());
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(getDomain());
        objectOutputStream.close();
        @NotNull final String base64 = Base64.getEncoder().encodeToString(byteArrayOutputStream.toByteArray());
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(base64.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

}
