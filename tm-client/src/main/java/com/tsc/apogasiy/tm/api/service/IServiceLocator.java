package com.tsc.apogasiy.tm.api.service;

import com.tsc.apogasiy.tm.endpoint.*;
import org.jetbrains.annotations.Nullable;

public interface IServiceLocator {

    @Nullable ICommandService getCommandService();

    @Nullable IAuthService getAuthService();

    @Nullable IPropertyService getPropertyService();

    @Nullable TaskEndpointService getTaskService();

    @Nullable TaskEndpoint getTaskEndpoint();

    @Nullable ProjectEndpointService getProjectService();

    @Nullable ProjectEndpoint getProjectEndpoint();

    @Nullable UserEndpointService getUserService();

    @Nullable UserEndpoint getUserEndpoint();

    @Nullable UserAdminEndpointService getUserAdminService();

    @Nullable UserAdminEndpoint getUserAdminEndpoint();

    @Nullable SessionEndpointService getSessionService();

    @Nullable SessionEndpoint getSessionEndpoint();

}
