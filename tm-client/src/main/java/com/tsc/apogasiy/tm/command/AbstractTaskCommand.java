package com.tsc.apogasiy.tm.command;

import com.tsc.apogasiy.tm.endpoint.Role;
import com.tsc.apogasiy.tm.endpoint.Task;
import com.tsc.apogasiy.tm.exception.entity.TaskNotFoundException;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @Override
    public Role[] roles() {
        return Role.values();
    }

    protected void showTask(@Nullable final Task task) {
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Status: " + task.getStatus());
    }

}
