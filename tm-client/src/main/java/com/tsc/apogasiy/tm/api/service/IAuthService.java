package com.tsc.apogasiy.tm.api.service;

import com.tsc.apogasiy.tm.endpoint.Role;
import com.tsc.apogasiy.tm.endpoint.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IAuthService {

    boolean isAuth();

    boolean isAdmin();

    void login(@Nullable final String login, @Nullable final String password);

    void logout();

    void checkRoles(@Nullable final Role... roles);

    @Nullable Session getSession();

    void setSession(@NotNull final Session session);

}