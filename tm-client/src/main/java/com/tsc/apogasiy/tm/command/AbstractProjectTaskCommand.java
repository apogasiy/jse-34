package com.tsc.apogasiy.tm.command;

import com.tsc.apogasiy.tm.endpoint.Project;
import com.tsc.apogasiy.tm.endpoint.Role;
import com.tsc.apogasiy.tm.endpoint.Task;
import com.tsc.apogasiy.tm.exception.entity.ProjectNotFoundException;
import com.tsc.apogasiy.tm.exception.entity.TaskNotFoundException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Optional;

public abstract class AbstractProjectTaskCommand extends AbstractCommand {

    @Override
    public Role[] roles() {
        return Role.values();
    }

    protected void showProjectTasks(@Nullable final Project project) {
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus());
        @NotNull List<Task> tasks = serviceLocator.getTaskEndpoint().findTaskByProjectIdTask(getSession(), project.getId());
        if (tasks.size() <= 0)
            throw new TaskNotFoundException();
        for (@NotNull final Task task : tasks) {
            System.out.println("Id: " + task.getId());
            System.out.println("Name: " + task.getName());
            System.out.println("Description: " + task.getDescription());
            System.out.println("Status: " + task.getStatus());
        }
    }

}
