package com.tsc.apogasiy.tm.command.project;

import com.tsc.apogasiy.tm.command.AbstractProjectCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public @NotNull String getCommand() {
        return "project-clear";
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return "Drop all projects";
    }

    @Override
    public void execute() {
        serviceLocator.getProjectEndpoint().clearUserProject(getSession());
    }

}
