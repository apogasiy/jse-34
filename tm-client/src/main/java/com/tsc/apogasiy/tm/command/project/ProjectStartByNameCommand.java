package com.tsc.apogasiy.tm.command.project;

import com.tsc.apogasiy.tm.command.AbstractProjectCommand;
import com.tsc.apogasiy.tm.endpoint.Project;
import com.tsc.apogasiy.tm.exception.entity.ProjectNotFoundException;
import com.tsc.apogasiy.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class ProjectStartByNameCommand extends AbstractProjectCommand {

    @Override
    public @NotNull String getCommand() {
        return "project-start-by-name";
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return "Start project by name";
    }

    @Override
    public void execute() {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectEndpoint().findByNameProject(getSession(), name);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        serviceLocator.getProjectEndpoint().startByNameProject(getSession(), name);
    }

}
