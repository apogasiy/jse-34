package com.tsc.apogasiy.tm.command.task;

import com.tsc.apogasiy.tm.command.AbstractTaskCommand;
import com.tsc.apogasiy.tm.endpoint.Task;
import com.tsc.apogasiy.tm.exception.entity.TaskNotFoundException;
import com.tsc.apogasiy.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class TaskFinishByIdCommand extends AbstractTaskCommand {

    @Override
    public @NotNull String getCommand() {
        return "task-finish-by-id";
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return "Finish task by id";
    }

    @Override
    public void execute() {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskEndpoint().findByIdTask(getSession(), id);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        serviceLocator.getTaskEndpoint().finishByIdTask(getSession(), id);
    }

}
