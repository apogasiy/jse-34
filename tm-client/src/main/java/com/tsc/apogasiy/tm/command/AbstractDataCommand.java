package com.tsc.apogasiy.tm.command;

import com.tsc.apogasiy.tm.dto.Domain;
import com.tsc.apogasiy.tm.endpoint.Role;
import com.tsc.apogasiy.tm.exception.entity.EntityNotFoundException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public abstract class AbstractDataCommand extends AbstractCommand {

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

    @NotNull
    public Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        if (!Optional.ofNullable(serviceLocator).isPresent())
            throw new EntityNotFoundException();
        domain.setProjects(serviceLocator.getProjectEndpoint().findAllProject(getSession()));
        domain.setTasks(serviceLocator.getTaskEndpoint().findAllTask(getSession()));
        domain.setUsers(serviceLocator.getUserAdminEndpoint().findAllUser(getSession()));
        return domain;
    }

    public void setDomain(@Nullable final Domain domain) {
        if (!Optional.ofNullable(serviceLocator).isPresent())
            throw new EntityNotFoundException();
        if (Optional.ofNullable(domain).isPresent()) {
            serviceLocator.getUserAdminEndpoint().clearUser(getSession());
            serviceLocator.getTaskEndpoint().clearTask(getSession());
            serviceLocator.getProjectEndpoint().clearUserProject(getSession());
            serviceLocator.getUserAdminEndpoint().addAllUser(getSession(), domain.getUsers());
            serviceLocator.getTaskEndpoint().addAllTask(getSession(), domain.getTasks());
            serviceLocator.getProjectEndpoint().addAllProject(getSession(), domain.getProjects());
            if (serviceLocator.getAuthService().isAuth())
                serviceLocator.getAuthService().logout();
        }
    }

}