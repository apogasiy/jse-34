package com.tsc.apogasiy.tm.command.data;

import com.tsc.apogasiy.tm.command.AbstractDataCommand;
import com.tsc.apogasiy.tm.dto.Domain;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.FileOutputStream;

public class DataXmlSaveJaxBCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String getCommand() {
        return "data-save-jaxb-xml";
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Save data to json by JAXB";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final Domain domain = getDomain();
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(serviceLocator.getPropertyService().getDTOXmlFileName());
        marshaller.marshal(domain, fileOutputStream);
        fileOutputStream.close();
    }

}

