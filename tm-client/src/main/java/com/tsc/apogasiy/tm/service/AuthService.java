package com.tsc.apogasiy.tm.service;

import com.tsc.apogasiy.tm.api.service.IAuthService;
import com.tsc.apogasiy.tm.endpoint.*;
import com.tsc.apogasiy.tm.exception.empty.EmptyLoginException;
import com.tsc.apogasiy.tm.exception.empty.EmptyPasswordException;
import com.tsc.apogasiy.tm.exception.entity.UserNotFoundException;
import com.tsc.apogasiy.tm.exception.system.AccessDeniedException;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class AuthService implements IAuthService {

    @Getter
    @Setter
    @Nullable
    private Session session;

    @Nullable
    private final UserAdminEndpoint userService;

    @Nullable
    private final SessionEndpoint sessionEndpoint;

    public AuthService(@NotNull UserAdminEndpointService userService, @NotNull SessionEndpointService sessionEndpointService) {
        this.userService = userService.getUserAdminEndpointPort();
        this.sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
    }

    @Override
    public boolean isAuth() {
        return (getSession() != null);
    }

    @Override
    public boolean isAdmin() {
        @Nullable final Role role = userService.findByIdUser(getSession(), getSession().getUserId()).getRole();
        return role.equals(Role.ADMIN);
    }

    @Override
    public void login(@Nullable final String login, @Nullable final String password) {
        if (!Optional.ofNullable(login).isPresent() || login.isEmpty())
            throw new EmptyLoginException();
        if (password == null || password.isEmpty())
            throw new EmptyPasswordException();
        setSession(sessionEndpoint.open(login, password));
        @NotNull final User user = Optional.ofNullable(userService.findByLoginUser(getSession(), login)).orElseThrow(UserNotFoundException::new);
        if (user.isLocked())
            throw new AccessDeniedException();
    }

    @Override
    public void logout() {
        if (!isAuth())
            throw new AccessDeniedException();
        setSession(null);
    }

    @Override
    public void checkRoles(final Role... roles) {
        if (roles == null || roles.length == 0)
            return;
        @NotNull final User user = userService.findByIdUser(getSession(), getSession().getUserId());
        if (user == null)
            throw new AccessDeniedException();
        @Nullable final Role role = user.getRole();
        if (role == null)
            throw new AccessDeniedException();
        for (@NotNull final Role item : roles) {
            if (item.equals(role))
                return;
        }
        throw new AccessDeniedException();
    }

}
