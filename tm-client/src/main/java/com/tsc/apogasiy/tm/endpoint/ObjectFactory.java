
package com.tsc.apogasiy.tm.endpoint;

import com.tsc.apogasiy.tm.endpoint.*;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.tsc.apogasiy.tm.endpoint package. 
 * &lt;p&gt;An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SetPasswordMyUser_QNAME = new QName("http://endpoint.server.apogasiy.tsc.com/", "setPasswordMyUser");
    private final static QName _SetPasswordMyUserResponse_QNAME = new QName("http://endpoint.server.apogasiy.tsc.com/", "setPasswordMyUserResponse");
    private final static QName _UpdateMyUser_QNAME = new QName("http://endpoint.server.apogasiy.tsc.com/", "updateMyUser");
    private final static QName _UpdateMyUserResponse_QNAME = new QName("http://endpoint.server.apogasiy.tsc.com/", "updateMyUserResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.tsc.apogasiy.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SetPasswordMyUser }
     * 
     */
    public SetPasswordMyUser createSetPasswordMyUser() {
        return new SetPasswordMyUser();
    }

    /**
     * Create an instance of {@link SetPasswordMyUserResponse }
     * 
     */
    public SetPasswordMyUserResponse createSetPasswordMyUserResponse() {
        return new SetPasswordMyUserResponse();
    }

    /**
     * Create an instance of {@link UpdateMyUser }
     * 
     */
    public UpdateMyUser createUpdateMyUser() {
        return new UpdateMyUser();
    }

    /**
     * Create an instance of {@link UpdateMyUserResponse }
     * 
     */
    public UpdateMyUserResponse createUpdateMyUserResponse() {
        return new UpdateMyUserResponse();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link AbstractOwnerEntity }
     * 
     */
    public AbstractOwnerEntity createAbstractOwnerEntity() {
        return new AbstractOwnerEntity();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetPasswordMyUser }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SetPasswordMyUser }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.server.apogasiy.tsc.com/", name = "setPasswordMyUser")
    public JAXBElement<SetPasswordMyUser> createSetPasswordMyUser(SetPasswordMyUser value) {
        return new JAXBElement<SetPasswordMyUser>(_SetPasswordMyUser_QNAME, SetPasswordMyUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetPasswordMyUserResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SetPasswordMyUserResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.server.apogasiy.tsc.com/", name = "setPasswordMyUserResponse")
    public JAXBElement<SetPasswordMyUserResponse> createSetPasswordMyUserResponse(SetPasswordMyUserResponse value) {
        return new JAXBElement<SetPasswordMyUserResponse>(_SetPasswordMyUserResponse_QNAME, SetPasswordMyUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateMyUser }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UpdateMyUser }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.server.apogasiy.tsc.com/", name = "updateMyUser")
    public JAXBElement<UpdateMyUser> createUpdateMyUser(UpdateMyUser value) {
        return new JAXBElement<UpdateMyUser>(_UpdateMyUser_QNAME, UpdateMyUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateMyUserResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UpdateMyUserResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.server.apogasiy.tsc.com/", name = "updateMyUserResponse")
    public JAXBElement<UpdateMyUserResponse> createUpdateMyUserResponse(UpdateMyUserResponse value) {
        return new JAXBElement<UpdateMyUserResponse>(_UpdateMyUserResponse_QNAME, UpdateMyUserResponse.class, null, value);
    }

}
