package com.tsc.apogasiy.tm.command.project;

import com.tsc.apogasiy.tm.command.AbstractProjectCommand;
import com.tsc.apogasiy.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ProjectCreateCommand extends AbstractProjectCommand {

    @Override
    public @NotNull String getCommand() {
        return "project-create";
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return "Create new project";
    }

    @Override
    public void execute() {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter Description");
        final String description = TerminalUtil.nextLine();
        serviceLocator.getProjectEndpoint().createProject(getSession(), name, description);
    }

}
