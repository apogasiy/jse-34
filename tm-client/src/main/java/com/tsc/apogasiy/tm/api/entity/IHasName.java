package com.tsc.apogasiy.tm.api.entity;

import org.jetbrains.annotations.Nullable;

public interface IHasName {

    @Nullable String getName();

    void setName(@Nullable final String name);

}
