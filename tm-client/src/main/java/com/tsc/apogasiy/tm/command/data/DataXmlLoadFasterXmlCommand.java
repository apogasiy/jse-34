package com.tsc.apogasiy.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.tsc.apogasiy.tm.command.AbstractDataCommand;
import com.tsc.apogasiy.tm.dto.Domain;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.nio.file.Files;
import java.nio.file.Paths;

public class DataXmlLoadFasterXmlCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String getCommand() {
        return "data-load-fasterxml-xml";
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Load data from xml by FasterXML";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final String xml = new String(Files.readAllBytes(Paths.get(serviceLocator.getPropertyService().getDTOXmlFileName())));
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        setDomain(objectMapper.readValue(xml, Domain.class));
    }

}
