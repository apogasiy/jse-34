package com.tsc.apogasiy.tm.component;

import com.tsc.apogasiy.tm.api.repository.*;
import com.tsc.apogasiy.tm.api.service.*;
import com.tsc.apogasiy.tm.command.AbstractCommand;
import com.tsc.apogasiy.tm.command.system.ExitCommand;
import com.tsc.apogasiy.tm.endpoint.*;
import com.tsc.apogasiy.tm.exception.system.UnknownCommandException;
import com.tsc.apogasiy.tm.repository.*;
import com.tsc.apogasiy.tm.service.*;
import com.tsc.apogasiy.tm.util.SystemUtil;
import com.tsc.apogasiy.tm.util.TerminalUtil;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;

public class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ILogService logService = new LogService();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final TaskEndpointService taskEndpointService = new TaskEndpointService();

    @NotNull
    private final UserAdminEndpointService userAdminEndpointService = new UserAdminEndpointService();

    @NotNull
    private final UserEndpointService userEndpointService = new UserEndpointService();

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userAdminEndpointService, sessionEndpointService);

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    public void start(final String... args) {
        initPID();
        displayWelcome();
        runArgs(args);
        initCommands();
        initFileScanner();
        logService.debug("Test environment");
        @Nullable String command = "";
        while (!command.equals(ExitCommand.NAME)) {
            try {
                System.out.println("ENTER COMMAND");
                command = TerminalUtil.nextLine();
                logService.command(command);
                runCommand(command);
                logService.info("Completed");
            } catch (Exception e) {
                logService.error(e);
            }
        }
        stopFileScanner();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initCommands() {
        @NotNull final Reflections reflections = new Reflections("com.tsc.apogasiy.tm.command");
        for (@NotNull final Class<? extends AbstractCommand> clazz : reflections.getSubTypesOf(AbstractCommand.class)) {
            if (!Modifier.isAbstract(clazz.getModifiers())) {
                try {
                    registry(clazz.newInstance());
                } catch (InstantiationException | IllegalAccessException e) {
                    logService.error(e);
                }
            }
        }
    }

    private void initFileScanner() {
        fileScanner.start();
    }

    private void stopFileScanner() {
        fileScanner.stop();
    }

    private void displayWelcome() {
        System.out.println("---Welcome to task manager---");
    }

    private boolean runArgs(final String[] args) {
        if (!Optional.ofNullable(args).isPresent() || args.length == 0)
            return false;
        @Nullable AbstractCommand command = commandService.getCommandByName(args[0]);
        if (!Optional.ofNullable(command).isPresent())
            throw new UnknownCommandException(args[0]);
        command.execute();
        return true;
    }

    private void runCommand(@Nullable final String command) {
        runCommand(command, true);
    }

    public void runCommand(@Nullable final String command, boolean checkRoles) {
        if (!Optional.ofNullable(command).isPresent() || command.isEmpty())
            return;
        @Nullable AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (!Optional.ofNullable(abstractCommand).isPresent())
            throw new UnknownCommandException(command);
        if (checkRoles) {
            @Nullable final Role[] roles = abstractCommand.roles();
            authService.checkRoles(roles);
        }
        abstractCommand.execute();
    }

    private void registry(@Nullable AbstractCommand command) {
        if (!Optional.ofNullable(command).isPresent()) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    @Override
    public @Nullable TaskEndpointService getTaskService() {
        return taskEndpointService;
    }

    @Override
    public @Nullable TaskEndpoint getTaskEndpoint() {
        return taskEndpointService.getTaskEndpointPort();
    }

    @Override
    public @Nullable ProjectEndpointService getProjectService() {
        return projectEndpointService;
    }

    @Override
    public @Nullable ProjectEndpoint getProjectEndpoint() {
        return projectEndpointService.getProjectEndpointPort();
    }

    @Override
    public @Nullable UserEndpointService getUserService() {
        return userEndpointService;
    }

    @Override
    public @Nullable UserEndpoint getUserEndpoint() {
        return userEndpointService.getUserEndpointPort();
    }

    @Override
    public @Nullable UserAdminEndpointService getUserAdminService() {
        return userAdminEndpointService;
    }

    @Override
    public @Nullable UserAdminEndpoint getUserAdminEndpoint() {
        return userAdminEndpointService.getUserAdminEndpointPort();
    }

    @Override
    public @Nullable SessionEndpointService getSessionService() {
        return sessionEndpointService;
    }

    @Override
    public @Nullable SessionEndpoint getSessionEndpoint() {
        return sessionEndpointService.getSessionEndpointPort();
    }

}
