package com.tsc.apogasiy.tm.command.task;

import com.tsc.apogasiy.tm.command.AbstractProjectTaskCommand;
import com.tsc.apogasiy.tm.endpoint.Project;
import com.tsc.apogasiy.tm.endpoint.Task;
import com.tsc.apogasiy.tm.exception.entity.ProjectNotFoundException;
import com.tsc.apogasiy.tm.exception.entity.TaskNotFoundException;
import com.tsc.apogasiy.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class TaskAddToProjectByIdProjectTaskCommand extends AbstractProjectTaskCommand {

    @Override
    public @NotNull String getCommand() {
        return "task-add-to-project-by-id";
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return "Add task to project by id";
    }

    @Override
    public void execute() {
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().getProjectEndpointPort().findByIdProject(getSession(), projectId);
        if (project == null)
            throw (new ProjectNotFoundException());
        System.out.println("Enter task id");
        final String taskId = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().getTaskEndpointPort().findByIdTask(getSession(), taskId);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        serviceLocator.getTaskEndpoint().bindTaskByIdTask(getSession(), projectId, taskId);
    }

}
