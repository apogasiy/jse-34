package com.tsc.apogasiy.tm.command.task;

import com.tsc.apogasiy.tm.command.AbstractTaskCommand;
import com.tsc.apogasiy.tm.endpoint.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class TaskListShowCommand extends AbstractTaskCommand {

    @Override
    public @NotNull String getCommand() {
        return "task-list";
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return "Show all tasks";
    }

    @Override
    public void execute() {
        final List<Task> tasks = serviceLocator.getTaskEndpoint().findAllByUserTask(getSession());
        for (@NotNull final Task task : tasks)
            showTask(task);
    }

}