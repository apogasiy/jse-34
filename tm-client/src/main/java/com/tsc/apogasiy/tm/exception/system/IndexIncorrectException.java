package com.tsc.apogasiy.tm.exception.system;

import com.tsc.apogasiy.tm.exception.AbstractException;

public class IndexIncorrectException extends AbstractException {

    public IndexIncorrectException() {
        super("Error! Index is incorrect!");
    }

}
