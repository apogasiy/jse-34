package com.tsc.apogasiy.tm.command.project;

import com.tsc.apogasiy.tm.command.AbstractProjectCommand;
import com.tsc.apogasiy.tm.endpoint.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class ProjectListShowCommand extends AbstractProjectCommand {

    @Override
    public @NotNull String getCommand() {
        return "project-list";
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return "Show all projects";
    }

    @Override
    public void execute() {
        final List<Project> projects = serviceLocator.getProjectEndpoint().findAllProject(getSession());
        for (@NotNull final Project project : projects) showProject(project);
    }

}
