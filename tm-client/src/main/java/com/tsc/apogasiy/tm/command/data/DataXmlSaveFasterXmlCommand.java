package com.tsc.apogasiy.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationModule;
import com.tsc.apogasiy.tm.command.AbstractDataCommand;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.FileOutputStream;
import java.text.SimpleDateFormat;

public class DataXmlSaveFasterXmlCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String getCommand() {
        return "data-save-fasterxml-xml";
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Save data to xml by FasterXML";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        objectMapper.registerModule(new JaxbAnnotationModule());    // Поддержка аннотаций JAXB
        objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"));
        @NotNull final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(getDomain());
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(serviceLocator.getPropertyService().getDTOXmlFileName());
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.close();
    }

}
