package com.tsc.apogasiy.tm.command.user;

import com.tsc.apogasiy.tm.command.AbstractUserCommand;
import com.tsc.apogasiy.tm.exception.system.AccessDeniedException;
import com.tsc.apogasiy.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public class UserUpdateByIdCommand extends AbstractUserCommand {

    @Override
    public @NotNull String getCommand() {
        return "user-update-by-id";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return "Update user info by id";
    }

    @Override
    public void execute() {
        final boolean isAuth = serviceLocator.getAuthService().isAuth();
        if (!isAuth)
            throw new AccessDeniedException();
        System.out.println("Enter user id:");
        final String id = TerminalUtil.nextLine();
        System.out.println("Enter last name:");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("Enter first name:");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter middle name:");
        final String middleName = TerminalUtil.nextLine();
        System.out.println("Enter email:");
        final String email = TerminalUtil.nextLine();
        if (getSession().getUserId().equals(id))
            serviceLocator.getUserEndpoint().updateMyUser(getSession(), lastName, firstName, middleName, email);
        else
            serviceLocator.getUserAdminEndpoint().updateByIdUser(getSession(), id, lastName, firstName, middleName, email);
    }

}
