package com.tsc.apogasiy.tm.command;

import com.tsc.apogasiy.tm.endpoint.Project;
import com.tsc.apogasiy.tm.endpoint.Role;
import com.tsc.apogasiy.tm.exception.entity.ProjectNotFoundException;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @Override
    public Role[] roles() {
        return Role.values();
    }

    protected void showProject(@Nullable final Project project) {
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus());
    }

}
