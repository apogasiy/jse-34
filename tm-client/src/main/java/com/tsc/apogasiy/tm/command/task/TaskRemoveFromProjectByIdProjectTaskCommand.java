package com.tsc.apogasiy.tm.command.task;

import com.tsc.apogasiy.tm.command.AbstractProjectTaskCommand;
import com.tsc.apogasiy.tm.endpoint.Project;
import com.tsc.apogasiy.tm.endpoint.Task;
import com.tsc.apogasiy.tm.exception.entity.ProjectNotFoundException;
import com.tsc.apogasiy.tm.exception.entity.TaskNotFoundException;
import com.tsc.apogasiy.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class TaskRemoveFromProjectByIdProjectTaskCommand extends AbstractProjectTaskCommand {

    @Override
    public @NotNull String getCommand() {
        return "task-remove-from-project-by-id";
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return "Removes task from project by id";
    }

    @Override
    public void execute() {
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectEndpoint().finishByIdProject(getSession(), projectId);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        System.out.println("Enter task id");
        final String taskId = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskEndpoint().findByIdTask(getSession(), taskId);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        serviceLocator.getTaskEndpoint().unbindTaskByIdTask(getSession(), projectId, taskId);
    }

}
