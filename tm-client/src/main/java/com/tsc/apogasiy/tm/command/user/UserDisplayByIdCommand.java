package com.tsc.apogasiy.tm.command.user;

import com.tsc.apogasiy.tm.command.AbstractUserCommand;
import com.tsc.apogasiy.tm.endpoint.User;
import com.tsc.apogasiy.tm.exception.entity.UserNotFoundException;
import com.tsc.apogasiy.tm.exception.system.AccessDeniedException;
import com.tsc.apogasiy.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

public class UserDisplayByIdCommand extends AbstractUserCommand {

    @Override
    public @NotNull String getCommand() {
        return "user-display-by-id";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return "Display user by id";
    }

    @Override
    public void execute() {
        final boolean isAuth = serviceLocator.getAuthService().isAuth();
        final boolean isAdmin = serviceLocator.getAuthService().isAdmin();
        if (!isAuth || !isAdmin)
            throw new AccessDeniedException();
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserAdminEndpoint().findByIdUser(getSession(), id);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        showUser(user);
    }

}
