package com.tsc.apogasiy.tm.command.user;

import com.tsc.apogasiy.tm.command.AbstractUserCommand;
import com.tsc.apogasiy.tm.endpoint.Role;
import com.tsc.apogasiy.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public class UserCreateCommand extends AbstractUserCommand {

    @Override
    public @NotNull String getCommand() {
        return "user-create";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return "Create new user";
    }

    @Override
    public void execute() {
        System.out.println("Enter login: ");
        final String login = TerminalUtil.nextLine();
        System.out.println("Enter password: ");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getUserAdminEndpoint().createUser(getSession(), login, password, Role.USER);
    }

}
