package com.tsc.apogasiy.tm.command.user;

import com.tsc.apogasiy.tm.command.AbstractUserCommand;
import com.tsc.apogasiy.tm.endpoint.User;
import com.tsc.apogasiy.tm.exception.empty.EmptyUserListException;
import com.tsc.apogasiy.tm.exception.system.AccessDeniedException;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class UserListCommand extends AbstractUserCommand {

    @Override
    public @NotNull String getCommand() {
        return "user-list";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return "Display list of users";
    }

    @Override
    public void execute() {
        final boolean isAuth = serviceLocator.getAuthService().isAuth();
        final boolean isAdmin = serviceLocator.getAuthService().isAdmin();
        if (!isAuth || !isAdmin)
            throw new AccessDeniedException();
        System.out.println("User list");
        final List<User> users = serviceLocator.getUserAdminEndpoint().findAllUser(getSession());
        if (users == null)
            throw new EmptyUserListException();
        for (@NotNull final User user : users)
            showUser(user);
    }

}
