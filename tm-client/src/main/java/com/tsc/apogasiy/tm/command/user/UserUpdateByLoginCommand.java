package com.tsc.apogasiy.tm.command.user;

import com.tsc.apogasiy.tm.command.AbstractUserCommand;
import com.tsc.apogasiy.tm.exception.system.AccessDeniedException;
import com.tsc.apogasiy.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public class UserUpdateByLoginCommand extends AbstractUserCommand {

    @Override
    public @NotNull String getCommand() {
        return "user-update-by-login";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return "Update user info by login";
    }

    @Override
    public void execute() {
        final boolean isAuth = serviceLocator.getAuthService().isAuth();
        if (!isAuth)
            throw new AccessDeniedException();
        System.out.println("Enter user login:");
        final String login = TerminalUtil.nextLine();
        System.out.println("Enter last name:");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("Enter first name:");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter middle name:");
        final String middleName = TerminalUtil.nextLine();
        System.out.println("Enter email:");
        final String email = TerminalUtil.nextLine();
        serviceLocator.getUserAdminEndpoint().updateByLoginUser(getSession(), login, lastName, firstName, middleName, email);
    }

}
