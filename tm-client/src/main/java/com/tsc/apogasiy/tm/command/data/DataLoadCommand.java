package com.tsc.apogasiy.tm.command.data;

import org.jetbrains.annotations.NotNull;

public class DataLoadCommand extends DataXmlLoadFasterXmlCommand {

    public static final String NAME = "data-load";

    @Override
    @NotNull
    public String getCommand() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Load backup from XML";
    }

}
