package com.tsc.apogasiy.tm.component;

import com.tsc.apogasiy.tm.command.data.DataBackupCommand;
import com.tsc.apogasiy.tm.command.data.DataLoadCommand;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Backup implements Runnable {

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final ScheduledExecutorService executorService;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.executorService = Executors.newSingleThreadScheduledExecutor();
    }

    @Override
    public void run() {
        save();
    }

    public void start() {
        executorService.scheduleWithFixedDelay(this, 0, bootstrap.getPropertyService().getAutosaveInterval(), TimeUnit.SECONDS);
    }

    public void stop() {
        executorService.shutdown();
    }

    @SneakyThrows
    public void load() {
        bootstrap.runCommand(DataLoadCommand.NAME, false);
    }

    @SneakyThrows
    public void save() {
        bootstrap.runCommand(DataBackupCommand.NAME, false);
    }

}
