package com.tsc.apogasiy.tm.command.user;

import com.tsc.apogasiy.tm.command.AbstractUserCommand;
import com.tsc.apogasiy.tm.endpoint.Role;
import com.tsc.apogasiy.tm.endpoint.User;
import com.tsc.apogasiy.tm.exception.entity.UserNotFoundException;
import com.tsc.apogasiy.tm.exception.system.AccessDeniedException;
import com.tsc.apogasiy.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Optional;

public class UserChangeRoleCommand extends AbstractUserCommand {

    @Override
    public @NotNull String getCommand() {
        return "user-change-role";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return "Changes user role";
    }

    @Override
    public void execute() {
        final boolean isAuth = serviceLocator.getAuthService().isAuth();
        final boolean isAdmin = serviceLocator.getAuthService().isAdmin();
        if (!isAuth || !isAdmin)
            throw new AccessDeniedException();
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserAdminEndpoint().findByIdUser(getSession(), id);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        System.out.println("Enter role:");
        System.out.println(Arrays.toString(Role.values()));
        final String roleValue = TerminalUtil.nextLine();
        final Role role = Role.valueOf(roleValue);
        serviceLocator.getUserAdminEndpoint().setRoleUser(getSession(), id, role);
    }

}
