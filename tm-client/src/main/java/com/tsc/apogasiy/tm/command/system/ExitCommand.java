package com.tsc.apogasiy.tm.command.system;

import com.tsc.apogasiy.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ExitCommand extends AbstractCommand {

    public static final String NAME = "exit";

    @Override
    public @NotNull String getCommand() {
        return NAME;
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return "Close application";
    }

    @Override
    public void execute() {
    }

}
