package com.tsc.apogasiy.tm.api.entity;

import com.tsc.apogasiy.tm.endpoint.Status;
import org.jetbrains.annotations.Nullable;

public interface IHasStatus {

    @Nullable Status getStatus();

    void setStatus(@Nullable final Status status);

}
